package eu.ms.codility.rest;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Małgorzata Sienkiewicz
 */
@RestController
public class HealthcheckController {

    @GetMapping(value = "/healthcheck", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity healthcheck(@RequestParam String format) {
        switch (format) {
            case "short":
                return ResponseEntity.ok().body("{\"status\"=\"OK\"}");
            case "full":
                return ResponseEntity.ok().body("{ \"currentTime\": \"" + time() + "\", \"application\": \"OK\" }");
            default:
                return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(value = "/healthcheck")
    public ResponseEntity<?> healthcheckPost() {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }

    @PutMapping(value = "/healthcheck")
    public ResponseEntity<?> healthcheckPut() {
        return ResponseEntity.status(405).build();
    }

    @DeleteMapping(value = "/healthcheck")
    public ResponseEntity<?> healthcheckDelete() {
        return ResponseEntity.status(405).build();
    }

    private String time() {
        return ZonedDateTime.now(ZoneId.of("Z")).format(DateTimeFormatter.ISO_DATE);
    }

}
