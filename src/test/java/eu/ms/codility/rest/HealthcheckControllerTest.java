package eu.ms.codility.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.regex.Pattern;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 *
 * @author Małgorzata Sienkiewicz
 */
@SpringBootTest
@AutoConfigureMockMvc
public class HealthcheckControllerTest {

    @Autowired
    private MockMvc mockMvc;

    public HealthcheckControllerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testHealthcheckShort() throws Exception {
        this.mockMvc.perform(get("/healthcheck?format=short")).andDo(print())
            .andExpect(status().isOk()).andExpect(content().json("{\"status\"=\"OK\"}"));
    }

    @Test
    public void testHealthcheckFull() throws Exception {
        this.mockMvc.perform(get("/healthcheck?format=full")).andDo(print())
            .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(new Match()));
    }

    @Test
    public void testHealthcheckBadRequest() throws Exception {
        this.mockMvc.perform(get("/healthcheck?format=aaaaa"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testHealthcheckBadRequest2() throws Exception {
        this.mockMvc.perform(get("/healthcheck"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testHealthcheckPut() throws Exception {
        this.mockMvc.perform(put("/healthcheck")).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testHealthcheckPost() throws Exception {
        this.mockMvc.perform(post("/healthcheck")).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testHealthcheckDelete() throws Exception {
        this.mockMvc.perform(delete("/healthcheck")).andExpect(status().isMethodNotAllowed());
    }

    public class Match implements Matcher {

        private final Pattern pattern;

        private Match() {
            String expeted = "^\\{ \"currentTime\": \"2020-07-[0-9]{2}Z\", \"application\": \"OK\" \\}$";
            this.pattern = Pattern.compile(expeted);

        }

        @Override
        public boolean matches(Object actual) {

            java.util.regex.Matcher matcher = pattern.matcher(actual.toString());
            return matcher.matches();
        }

        @Override
        public void describeMismatch(Object actual, Description mismatchDescription) {
        }

        @Override
        public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {
        }

        @Override
        public void describeTo(Description description) {
        }

    }
}
