package eu.ms.codility;

import static org.assertj.core.api.Assertions.assertThat;

import eu.ms.codility.rest.HealthcheckController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author Małgorzata Sienkiewicz
 */
@SpringBootTest
public class ApplicationTests {

    @Autowired
    private HealthcheckController controller;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }

}
